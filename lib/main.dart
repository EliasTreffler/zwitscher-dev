import 'package:flutter/material.dart';

import 'classes/stundenplan.dart';
import 'classes/daystillsummer.dart';
import 'screens/loading.dart';

void main() {
  runApp(MaterialApp(
      //for testing purposes:
      // initialRoute: '/home',
      routes: {
        '/': (context) => Splashscreen(),
        '/home': (context) => Home(),
        /* '/location': (context) => location(), */
      }
      /*
    onPressed: () {
      navigator.pushNamed(context, '/location');
    },
    */
      ));
}

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Zwitscher'),
        /* padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0), */
        centerTitle: true,
        backgroundColor: Color(0xff15265c),
        /* leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {},
        ),
        */
      ),
      drawer: Drawer(
        backgroundColor: Colors.blueGrey[900],
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Card(
              color: Colors.blueGrey[700],
              child: ListTile(
                leading: const Icon(Icons.calendar_month, color: Colors.white),
                textColor: Colors.white,
                onTap: () {
                  /*
                   Navigator.pushNamed(context: , '/location'),
                   */
                  Navigator.pop(context);
                },
                title: Transform(
                  transform: Matrix4.translationValues(-20, 0.0, 0.0),
                  child: const Text('Text 1'),
                ),
              ),
            ),
            Card(
              color: Colors.blueGrey[700],
              child: ListTile(
                leading: const Icon(Icons.calendar_month, color: Colors.white),
                onTap: () {
                  /*
                   Navigator.pushNamed(context: , '/location'),
                   */
                  Navigator.pop(context);
                },
                textColor: Colors.white,
                title: Transform(
                  transform: Matrix4.translationValues(-20, 0.0, 0.0),
                  child: const Text('Text 2'),
                ),
              ),
            ),
            Card(
              color: Colors.blueGrey[700],
              child: ListTile(
                leading: const Icon(Icons.calendar_month, color: Colors.white),
                onTap: () {
                  /*
                   Navigator.pushNamed(context: , '/location'),
                   */
                  Navigator.pop(context);
                },
                textColor: Colors.white,
                title: Transform(
                  transform: Matrix4.translationValues(-20, 0.0, 0.0),
                  child: const Text('Text 3'),
                ),
              ),
            ),
          ],
        ),
      ),
      body: Container(
        color: Color(0xff061744),
        width: MediaQuery.of(context).size.width,
        child: const Padding(
          padding: EdgeInsets.all(30.0),
          child: Column(
            children: <Widget>[
              DaysTillSummer(),
              Stundenplan(),
            ],
          ),
        ),
      ),
    );
  } //widget
}
