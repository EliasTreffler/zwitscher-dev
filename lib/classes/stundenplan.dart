import 'package:flutter/material.dart';

class Stundenplan extends StatelessWidget {
  const Stundenplan({super.key});

  @override
  Widget build(BuildContext context) {
    /* return Card(
  elevation: 1.0,
  color: Colors.blueGrey[800],
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15.0),
  ),
  */

    return Card(

      elevation: 1.0,
      color: Colors.blueGrey[800],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),

      /* child: ListTile(
        leading: Icon(Icons.calendar_month, color: Colors.white),
        textColor: Colors.white,
        title: Transform(
          transform: Matrix4.translationValues(-20, 0.0, 0.0),
          child: Text('Text 2'),
        ),
      ),
      */

      /* UPPER LOWER DIVIDER */
      child: Column(
        children: [

          /* Originale Obere Häflte Ab Hier
          Row(
            children: [
              
              Padding (
                padding: EdgeInsets.all(30.0),
                child: Icon(
                 Icons.calendar_month,
                  color: Colors.white,
                 size: 24.0,
              ),
              ),

              Text(
                'Montag',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                )
              ),

            ],
          ),
          */

          Container(
            padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
            /* decoration: BoxDecoration(
              color: Colors.blueGrey[800],
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                 BoxShadow(
                   color: Color(0xFF303030),
                    spreadRadius: 5.0,
                    blurRadius: 7,
                   offset: Offset(2, 0), // changes position of shadow
                  ),
              ],
            ),
            */
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  Text(
                      'Montag',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 50,
                      ),
                    ),

          /*
                  Text(
                      '32.13.01',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
          */

                ],

              ),
            ),


          const Divider(
              height: 5.0,
              color: Colors.white,
            ),

          /* Originale Untere Hälfte Ab Hier
          Row(
            children: [

          ],
          ), */

          const StundenplanItem(),

        ],
      ),

    );
  }
}

class StundenplanItem extends StatelessWidget {
  const StundenplanItem({super.key});


  @override
  Widget build(BuildContext context) {

  return const ListTile(

    title: Text("Hello World!"),

  );

}
}