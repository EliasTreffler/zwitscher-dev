import 'package:flutter/material.dart';

class DaysTillSummer extends StatelessWidget {
  const DaysTillSummer({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          gradient: LinearGradient(
            colors: [Color(0xff9119fd), Color(0xffee6c6c)],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: Column(
          children: [
            /* Container(

        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 30),
        child: Column(
          children: [
      */

            const Text(
              '365 Days',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 50,
                color: Colors.white,
              ),
            ),
            const Divider(
              height: 7.0,
              color: Colors.white,
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(30, 10, 30, 0),
              child: const Text(
                'bis zu den Sommerferien',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                ),
              ),
            ),
            Progressbar(),
            Container(
              padding: const EdgeInsets.fromLTRB(30, 20, 30, 0),
              child: const Text(
                'Zeit bis zu den Nächsten Ferien:',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: Progressbar(),
            ),
          ],
        ));
  }
}

class Progressbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      //width: MediaQuery.of(context).size.width,
      //width: double.infinity,
      margin: const EdgeInsets.fromLTRB(15, 10, 15, 0),
      padding: const EdgeInsets.fromLTRB(3.0, 3.0, 3.0, 3.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        /* gradient: LinearGradient(
                    colors: [Color(0xffb700ec), Color(0xfff601cb)],
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                  ) */
      ),
      child: SizedBox(
        //margin: const EdgeInsets.fromLTRB(3.0, 3.0, 3.0, 3.0),
        //alignment: Alignment.centerLeft,
        height: 20,
        width: ((MediaQuery.of(context).size.width * 0.5) - 18 - 18 - 10),
        //width: 100 * 0.5,
        child: Container(
          //width: MediaQuery.of(context).size.width * 0.5,
          //width: ((MediaQuery.of(context).size.width - 6.0 - 30.0 - 5.0) * 0.5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              gradient: const LinearGradient(
                  colors: [Color(0xffb700ec), Color(0xfff601cb)],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight)),
        ),
      ),
    );
  }
}
